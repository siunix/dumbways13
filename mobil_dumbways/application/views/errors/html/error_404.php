<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title><?= APP_NAME ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?= config_item("base_url")?>assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= config_item("base_url")?>assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= config_item("base_url")?>assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="<?= config_item("base_url")?>assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?= config_item("base_url")?>assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?= config_item("base_url")?>assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= config_item("base_url")?>assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="lockscreen clearfix center-block" style="width:40%; align:center-block">
					<img src="<?= config_item("base_url") ?>assets/img/image_404.png" alt="404 Not found" width="100%" style="margin-bottom:20px">
					<p align="center">Oppsss!, Sorry we can't find page your looking for</p>
					<a href="<?= config_item("base_url") ?>" class="btn btn-info btn-block">Back to homepage</a>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
