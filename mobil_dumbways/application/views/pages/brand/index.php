<!doctype html>
<html lang="en">

<head>
    <?php
    $this->load->view("templates/head");
    ?>
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <?php
        $this->load->view("templates/navbar");
        ?>
        <!-- END NAVBAR -->

        <!-- LEFT SIDEBAR -->
        <?php
        $this->load->view("templates/sidebar");
        ?>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="panel panel-headline col-lg-6 col-md-6 col-sm-12">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="panel-title">Brand</h3>
                            <div class="right">
                                <button type="button" id="new" onclick="load_detail('')"><span class="lnr lnr-plus-circle"></span> New</button>
                                <button type="button" id="back" onclick="load_list('')"><span class="lnr lnr-chevron-left"></span> Back</button>
                            </div>
                        </div>
                        <div class="panel-body" id="content-body">
                            ...
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <?php
        $this->load->view("templates/footer");
        ?>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <?php
    $this->load->view("templates/scripts");
    ?>

    <script>
        $(document).ready(function() {
            load_list();
        })

        function load_list() {
            ajax_get(
                "<?= base_url("brand/ajax_list") ?>", {},
                function(resp) {
                    $("#back").hide();
                    $("#new").show();
                    $("#panel-title").text("Brand");
                    $("#content-body").html(resp);
                }
            );
        }

        function load_detail(hash_id) {
            ajax_get(
                "<?= base_url("brand/ajax_detail/") ?>" + hash_id, {},
                function(resp) {
                    $("#new").hide();
                    $("#back").show();
                    $("#content-body").html(resp);
                }
            );
        }

        function save() {
            var form_data = $("#form-detail").serializeArray();
            ajax_post(
                "<?= base_url("brand/ajax_save") ?>",
                form_data,
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            load_list();
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error");
                    }
                }
            );
        }

        function confirm_delete(hash_id) {
            var confirm_del = confirm("are you sure ?");
            if (!confirm_del) return false;

            ajax_get(
                "<?= base_url("brand/ajax_delete/") ?>" + hash_id, {},
                function(resp) {
                    try {
                        var json = JSON.parse(resp);
                        if (json.is_success == 1) {
                            show_toast("Success", json.message, "success");
                            load_list();
                        } else {
                            show_toast("Error", json.message, "error");
                        }
                    } catch (error) {
                        show_toast("Error", "Application response error", "error");
                    }
                }
            );
        }
    </script>
</body>

</html>