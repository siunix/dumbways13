<!doctype html>
<html lang="en">

<head>
    <?php
    $this->load->view("templates/head");
    ?>
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <?php
        $this->load->view("templates/navbar");
        ?>
        <!-- END NAVBAR -->

        <!-- LEFT SIDEBAR -->
        <?php
        $this->load->view("templates/sidebar");
        ?>
        <!-- END LEFT SIDEBAR -->
        <!-- MAIN -->
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="panel">
                        <?php
                        printr($final);
                        ?>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
        <!-- END MAIN -->
        <?php
        $this->load->view("templates/footer");
        ?>
    </div>
    <!-- END WRAPPER -->
    <!-- Javascript -->
    <?php
    $this->load->view("templates/scripts");
    ?>
</body>

</html>