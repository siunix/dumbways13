<?php
$hash_id = "";
$name = "";
$email = "";
$address = "";

if (count($detail) > 0) {
    $hash_id = isset($detail["id"]) ? md5($detail["id"]) : "";
    $name = isset($detail["name"]) ? ($detail["name"]) : "";
    $email = isset($detail["email"]) ? ($detail["email"]) : "";
    $address = isset($detail["address"]) ? ($detail["address"]) : "";
}
?>

<form onsubmit="return false" id="form-detail">
    <input type="hidden" name="hash_id" value="<?= $hash_id ?>">

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" id="name" name="name" class="form-control" value="<?= $name ?>">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" id="email" name="email" class="form-control" value="<?= $email ?>">
    </div>

    <div class="form-group">
        <label for="address">address</label>
        <input type="text" id="address" name="address" class="form-control" value="<?= $address ?>">
    </div>
</form>

<hr>
<a href="javascript:void(0);" onclick="confirm_delete('<?= $hash_id ?>')">Delete</a>
<div class="pull-right">
    <?php
    $text = "Back";
    if (empty($hash_id)) $text = "Cancel";
    ?>
    <button class="btn btn-secondary" onclick="load_list()"><?= $text ?></button>
    <button class="btn btn-primary" onclick="save()">Save</button>
</div>