<?php
if (count($list) > 0) {
    ?>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($list as $key => $value) {
                        ?>
                    <tr>
                        <td><?= $value["name"] ?></td>
                        <td><?= $value["email"] ?></td>
                        <td><?= $value["address"] ?></td>
                        <td>
                            <a href="javascript:void(0);" onclick="load_detail('<?= md5($value['id']) ?>')">Detail</a>
                        </td>
                    </tr>
                <?php
                    }
                    ?>
            </tbody>
        </table>
    </div>
<?php
} else {
    echo "No data";
}
?>