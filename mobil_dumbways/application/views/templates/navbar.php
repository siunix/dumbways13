<?php
$session_data = $this->session->userdata(APP_SESSION_NAME);
$session_user_login_hash_id = isset($session_data["hash_id"]) ? $session_data["hash_id"] : "";
$fullname = isset($session_data["fullname"]) ? $session_data["fullname"] : "";
$username = isset($session_data["username"]) ? $session_data["username"] : "";
$email = isset($session_data["email"]) ? $session_data["email"] : "";
$user_level = isset($session_data["user_level"]) ? $session_data["user_level"] : "";
$allow_user_login = isset($session_data["allow_user_login"]) ? (int) $session_data["allow_user_login"] : "0";
$allow_user_menu = isset($session_data["allow_user_menu"]) ? (int) $session_data["allow_user_menu"] : "0";
$allow_user_level = isset($session_data["allow_user_level"]) ? (int) $session_data["allow_user_level"] : "0";
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="<?= site_url() ?>"><h3><b><?= APP_NAME ?></b></h3></a>
    </div>
    <div class="container-fluid">
    </div>
</nav>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reset Password</h4>
            </div>
            <div class="modal-body">
                <form onsubmit="return false" id="form-reset-password">
                    <input type="hidden" name="hash_id" value="<?= $session_user_login_hash_id ?>">

                    <div class="form-group">
                        <label for="old_password">Old Password</label>
                        <input type="password" id="old_password" name="old_password" class="form-control" placeholder="Old password">
                    </div>

                    <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input type="password" id="new_password" name="new_password" class="form-control" placeholder="New password">
                    </div>

                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Confirm password">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" onclick="do_reset_password()" class="btn btn-primary">Reset</button>
            </div>
        </div>

    </div>
</div>