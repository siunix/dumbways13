<?php
?>
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="<?= site_url() ?>"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                <li><a href="<?= site_url("customer") ?>"><i class="lnr lnr-users"></i> <span>Customer</span></a></li>
                <li><a href="<?= site_url("brand") ?>"><i class="lnr lnr-list"></i> <span>Brand</span></a></li>
            </ul>
        </nav>
    </div>
</div>