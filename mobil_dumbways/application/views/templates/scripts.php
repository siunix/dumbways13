<!-- Javascript -->
<script src="<?= base_url("assets/vendor/jquery/jquery.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/bootstrap/js/bootstrap.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/chartist/js/chartist.min.js") ?>"></script>
<script src="<?= base_url("assets/scripts/klorofil-common.js") ?>"></script>
<script src="<?= base_url("assets/vendor/toastr/toastr.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/dropify/dropify.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/select2/select2.min.js") ?>"></script>
<script src="<?= base_url("assets/vendor/swal/sweetalert2.js") ?>"></script>
<script>
    var status_code = {
        200: function() { },
        201: function(xhr) {  },
        400: function(xhr) {
            try {                        
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
            } catch (error) {
                show_toast("Error","Application Response Error", "error");
            }
        },
        401: function(xhr) {
            try {                 
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
                
                window.location.href = '<?= base_url("login") ?>';
            } catch (error) {
                show_toast("Error","Application Response Error", "error");
            }

        },
        402: function(xhr) { console.log(text + ", " + status + ", ") },
        403: function(xhr) { 
            try {                 
                var response_text = JSON.parse(xhr.responseText);
                show_toast(xhr.statusText, response_text.message, "error");
            } catch (error) {
                show_toast("Error","Application Response Error", "error");
            }
        },
        404: function(xhr) { console.log(xhr) },
        405: function(xhr) { console.log(text + ", " + status + ", ") },
        500: function(xhr) {                     
            try {                                                
                show_toast(xhr.statusText, "Server Response Error", "error");
            } catch (error) {
                show_toast("Error","Application Response Error", "error");
            }
        }
    };

    function ajax_get(url, data, resp_) {
        $.ajax({
            url: url,
            type: "GET",
            data: data,
            beforeSend: function(xhr) {
                $("#loader").show();
            },
            complete: function(jqXHR, textStatus) {
                $("#loader").hide();
            },
            statusCode: {
                200: function() {},
                201: function(xhr) {},
                400: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                },
                401: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                        setTimeout(function() {
                            go_to_login_page();
                        }, 2300);
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }

                },
                402: function(xhr) {
                    console.log(text + ", " + status + ", ")
                },
                403: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                },
                404: function(xhr) {
                    console.log(xhr)
                },
                405: function(xhr) {
                    console.log(text + ", " + status + ", ")
                },
                500: function(xhr) {
                    try {
                        show_toast(xhr.statusText, "Server Response Error", "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                }
            },
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function ajax_post(url, data, resp_) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            beforeSend: function(xhr) {
                $("#loader").show();
            },
            complete: function(jqXHR, textStatus) {
                $("#loader").hide();
            },
            statusCode: {
                200: function() {},
                201: function(xhr) {},
                400: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                },
                401: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                        setTimeout(function() {
                            go_to_login_page();
                        }, 2300);
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                        go_to_login_page();
                    }

                },
                402: function(xhr) {
                    console.log(text + ", " + status + ", ")
                },
                403: function(xhr) {
                    try {
                        var response_text = JSON.parse(xhr.responseText);
                        show_toast(xhr.statusText, response_text.message, "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                },
                404: function(xhr) {
                    console.log(xhr)
                },
                405: function(xhr) {
                    console.log(text + ", " + status + ", ")
                },
                500: function(xhr) {
                    try {
                        show_toast(xhr.statusText, "Server Response Error", "error");
                    } catch (error) {
                        show_toast("Error", "Application Response Error", "error");
                    }
                }
            },
            success: function(resp, status) {
                resp_(resp);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $("#loader").hide();
            }
        });
    }

    function ajax_post_file(url, data, resp_){
      var ajax = $.ajax({
          url: url, 
          type: "POST",
          enctype: 'multipart/form-data',
          data: data, 
          processData: false,  // Important!
          contentType: false,
          cache: false,
          timeout: 600000,
          beforeSend: function (xhr) {   
              $("#loader").show();
          },
          complete: function (jqXHR, textStatus) {
              $("#loader").hide();
          },
          statusCode: status_code,
          success: function(resp, status) {
              resp_(resp);
          },
          error: function (jqXHR, textStatus, errorThrown) {                
              $("#loader").hide();
          }
      });        
    }

    function go_to_login_page() {
        window.location.href = "<?= base_url("login/do_logout") ?>";
    }

    function do_reset_password() {
        var form_data = $("#form-reset-password").serializeArray();
        ajax_post(
            "<?= base_url("user_login/ajax_do_reset_password") ?>",
            form_data,
            function(resp) {
                try {
                    var json = JSON.parse(resp);
                    if (json.is_success == 1) {
                        show_toast("Success", json.message, "success");
                        setTimeout(function() {
                            go_to_login_page();
                        }, 5300);
                    } else {
                        show_toast("Error", json.message, "error");
                    }
                } catch (error) {
                    show_toast("Error", "Application response error");
                }
            }
        );
    }

    function show_toast(title, message, type) {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr[type](message, title);
    }

    function confirm_logout() {
        var confirm = alert("Are you sure want to logout ?");
        if(confirm) {
            go_to_login_page;
        } else {
            return false;
        }
    }
</script>