<?php

class MY_Controller extends CI_Controller {
    
    private $is_ajax;

    public function __construct() {
        parent::__construct();

        $this->is_ajax = false;

        if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest") {
            $this->is_ajax = true;
        }
    }


}
