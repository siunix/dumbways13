<?php
class M_brand extends MY_Model {
    private $brand_engine;

    public function __construct() {
        parent::__construct();

        $this->brand_engine = new brand_engine();
    }


    function brand_get_list() {
        $res = $this->brand_engine->brand_get_list();

        return $res;
    }

    function brand_get($hash_id = "") {
        if(empty($hash_id)) return array();
        
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->brand_engine->brand_get_list($filters);

        if(count($res) == 0) return array();
        $res = $res[0];

        return $res;
    }

    function brand_save() {
        $id = 0;

        $hash_id = !empty($this->input->post("hash_id")) ? $this->input->post("hash_id") : "";
        $name = !empty($this->input->post("name")) ? $this->input->post("name") : "";
        
        // Jika edit (hash_id != "")
        // check dan validasi id
        if(!empty($hash_id)) {
            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->brand_engine->brand_get_list($filters);
            if(count($res) == 0) return set_http_response_error(201, "Invalid brand");
            $res = $res[0];
            $id = $res["id"];

        }

        if(empty($name)) return set_http_response_error(201, "Invalid brand name");

        // check duplicate brand name
        $filters = array();
        $filters["name"] = $name;
        $res = $this->brand_engine->brand_get_list($filters);
        if(count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res["id"]);
            if($hash_id != $current_hash_id) return set_http_response_error(201, "duplicate brand name");
        }
        
        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["name"] = !empty($name) ? trim($name) : "";
 
        $this->db->trans_begin();
        try {
            $res = $this->brand_engine->brand_save($save_data);
            if((int)$res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(201, "Failed to save brand data");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(201, "Failed to save brand data");
        }
    }

    function brand_delete($hash_id = ""){
        if(empty($hash_id)) return set_http_response_error(201, "Failed to delete brand");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->brand_engine->brand_get_list($filters);
        if(count($res) == 0) return set_http_response_error(201, "Failed to delete brand");
        $res = $res[0];
        $id = (int) $res["id"];

        $this->db->trans_begin();
        try {
            $res = $this->brand_engine->brand_delete($id);
            if((int)$res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(201, "Failed to delete brand");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(201, "Fail to delete brand");
        }
    }

}