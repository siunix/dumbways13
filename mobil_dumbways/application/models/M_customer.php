<?php
class M_customer extends MY_Model {
    private $customer_engine;

    public function __construct() {
        parent::__construct();

        $this->customer_engine = new customer_engine();
    }


    function customer_get_list() {
        $res = $this->customer_engine->customer_get_list();

        return $res;
    }

    function customer_get($hash_id = "") {
        if(empty($hash_id)) return array();
        
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->customer_engine->customer_get_list($filters);

        if(count($res) == 0) return array();
        $res = $res[0];

        return $res;
    }

    function customer_save() {
        $id = 0;

        $hash_id = !empty($this->input->post("hash_id")) ? $this->input->post("hash_id") : "";
        $name = !empty($this->input->post("name")) ? $this->input->post("name") : "";
        $email = !empty($this->input->post("email")) ? $this->input->post("email") : "";
        $address = !empty($this->input->post("address")) ? $this->input->post("address") : "";
        
        // Jika edit (hash_id != "")
        // check dan validasi id
        if(!empty($hash_id)) {
            $filters = array();
            $filters["hash_id"] = $hash_id;
            $res = $this->customer_engine->customer_get_list($filters);
            if(count($res) == 0) return set_http_response_error(201, "Invalid customer");
            $res = $res[0];
            $id = $res["id"];

        }

        if(empty($name)) return set_http_response_error(201, "Invalid customer name");
        if(empty($email)) return set_http_response_error(201, "Invalid customer email");
        if(empty($address)) return set_http_response_error(201, "Invalid customer address");

        // check duplicate customer email
        $filters = array();
        $filters["email"] = $email;
        $res = $this->customer_engine->customer_get_list($filters);
        if(count($res) > 0) {
            $res = $res[0];
            $current_hash_id = md5($res["id"]);
            if($hash_id != $current_hash_id) return set_http_response_error(201, "duplicate customer email");
        }
        
        $save_data = array();
        $save_data["id"] = (int) $id;
        $save_data["name"] = !empty($name) ? trim($name) : "";
        $save_data["email"] = !empty($email) ? trim($email) : "";
        $save_data["address"] = !empty($address) ? trim($address) : "";

        $this->db->trans_begin();
        try {
            $res = $this->customer_engine->customer_save($save_data);
            if((int)$res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(201, "Failed to save customer data");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(201, "Failed to save customer data");
        }
    }

    function customer_delete($hash_id = ""){
        if(empty($hash_id)) return set_http_response_error(201, "Failed to delete customer");

        // **
        // Check hash_id
        $filters = array();
        $filters["hash_id"] = $hash_id;
        $res = $this->customer_engine->customer_get_list($filters);
        if(count($res) == 0) return set_http_response_error(201, "Failed to delete customer");
        $res = $res[0];
        $id = (int) $res["id"];

        $this->db->trans_begin();
        try {
            $res = $this->customer_engine->customer_delete($id);
            if((int)$res == 0) {
                $this->db->trans_rollback();
                return set_http_response_error(201, "Failed to delete customer");
            }

            $this->db->trans_commit();
            return set_http_response_success("Success");
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return set_http_response_error(201, "Fail to delete customer");
        }
    }

}