<?php

class Db_engine {

    protected $db;

    public function __construct() {
        $ci =& get_instance();
        $this->db = $ci->db;
    }

    function get_id_by_hash_id($hash_id = "", $table_name = "") {
        if(empty($hash_id)) return 0;
        if(trim($hash_id) == "0") return 0;
        if(empty($table_name)) return 0;

        $this->db->select("id");
        $this->db->from($table_name);
        $this->db->where("md5(id)", $hash_id);
        $res = $this->db->get()->result_array();

        if(count($res) == 0) return 0;
        $res = $res[0];

        return (int)$res['id'];
    }
}