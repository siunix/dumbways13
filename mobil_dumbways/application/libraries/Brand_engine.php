<?php
class Brand_engine extends Db_engine {
    public function __construct(){
        parent::__construct();
    }


    function brand_get_list($filters = array()) {
        $this->db->select("*");
        $this->db->from("brand");

        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int)$value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'name':
                    $this->db->where("name", trim($value));
                    break;
                default:
                    break;
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function brand_save($data = array()) {
        $id = (int) $data["id"];
        $name = trim($data["name"]);

        $this->db->set(array(
            "id" => $id,
            "name" => $name,
        ));

        $this->db->replace("brand");

        if($id == 0) return $this->db->insert_id();
        return $id;
    }

    function brand_delete($id = 0) {
        if((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete("brand");

        return $id;
    }
    // ====================================================
    // ** END of brand
    // ====================================================

}