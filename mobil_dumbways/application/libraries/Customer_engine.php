<?php
class Customer_engine extends Db_engine {
    public function __construct(){
        parent::__construct();
    }


    function customer_get_list($filters = array()) {
        $this->db->select("*");
        $this->db->from("customer");

        foreach ($filters as $key => $value) {
            switch ($key) {
                case 'id':
                    $this->db->where("id", (int)$value);
                    break;
                case 'hash_id':
                    $this->db->where("md5(id)", trim($value));
                    break;
                case 'name':
                    $this->db->where("name", trim($value));
                    break;
                case 'email':
                    $this->db->where("email", trim($value));
                    break;
                case 'address':
                    $this->db->where("address", trim($value));
                    break;
                default:
                    break;
            }
        }

        $res = $this->db->get()->result_array();
        return $res;
    }

    function customer_save($data = array()) {
        $id = (int) $data["id"];
        $name = trim($data["name"]);
        $email = trim($data["email"]);
        $address = trim($data["address"]);

        $this->db->set(array(
            "id" => $id,
            "name" => $name,
            "email" => $email,
            "address" => $address,
        ));

        $this->db->replace("customer");

        if($id == 0) return $this->db->insert_id();
        return $id;
    }

    function customer_delete($id = 0) {
        if((int)$id == 0) return 0;

        $this->db->where("id", $id);
        $this->db->delete("customer");

        return $id;
    }
    // ====================================================
    // ** END of customer
    // ====================================================

}