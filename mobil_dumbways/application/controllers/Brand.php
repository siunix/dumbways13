<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends MY_Controller{

    private $is_ajax;

    // **
    // Structures
    // -- Contruct
    // -- Index
    // -- List
    // -- Detail
    // -- Save
    // -- Delete
    // -- Next Optional Method
    
    function __construct(){
        parent::__construct();

        $this->load->model("m_brand");

        $this->is_ajax = false;
        if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && trim($_SERVER["HTTP_X_REQUESTED_WITH"]) == "XMLHttpRequest"){
            $this->is_ajax = true;
        }
    }

    function index() {
        //-- Index
        $this->load->view("pages/brand/index");
    }

    function ajax_list(){
        //-- List
        if($this->is_ajax == false) redirect_url("");
        
        $data = array();
        $data["list"] = $this->m_brand->brand_get_list();

        $this->load->view("pages/brand/ajax_list", $data);
    }

    function ajax_detail($hash_id = "") {
        //-- Detail
        if($this->is_ajax == false) redirect_url("");

        $data = array();
        $data["detail"] = $this->m_brand->brand_get($hash_id);

        $this->load->view("pages/brand/ajax_detail", $data);
    }

    function ajax_save() {
        //-- Save
        if($this->is_ajax == false) redirect_url("");

        $res = $this->m_brand->brand_save();

        ob_clean();
        echo json_encode($res);
    }

    function ajax_delete($hash_id = "") {
        //-- Delete
        if($this->is_ajax == false) redirect_url("");
        
        $res = $this->m_brand->brand_delete($hash_id);

        ob_clean();
        echo json_encode($res);
    }
}
