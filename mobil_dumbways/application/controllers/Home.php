<?php

class Home extends MY_Controller {
    function __construct() {
        parent::__construct();

        // $this->load->model("m_home");
    }

    function index() {
        //-- Index

        $data = array();
        $this->load->view("pages/home/index", $data);
    }
}