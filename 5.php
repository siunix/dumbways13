<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Dumbways Batch 13 Kloter 4 - Abdillah F.</title>

    <style>
        .footer {
            position: fixed;
            padding: 10px;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #e91e63;
            color: white;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h3>Soal 5 Dumbways Batch 13 Kloter 14</h3>
                <hr>
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="text">Masukkan Text</label>
                        <input type="text" id="text" name="text" class="form-control">
                        <small class="form-text text-muted">Text yang ingin di enkripsi/dekripsi</small>
                    </div>
                    <br>
                    <input class="btn btn-primary" type="submit" name="submit" value="Urutkan">
                </form>
            </div>
        </div>
        <br>
        <hr>
        <code><b>Result</b></code>
        <div>
            <samp>
                <?php
                $text = isset($_POST["text"]) ? $_POST["text"] : "";

                $text_urutan = urutkanString($text);

                echo "Text awal (sebelum di urutkan) : $text";
                echo "<br>";
                echo "Text awal (setelah di urutkan) : " . implode($text_urutan);
                

                function urutkanString($text = "") {
                    $text_array = str_split($text);

                    // konversi huruf ke angka
                    $row = array();
                    foreach ($text_array as $index => $value) {
                        array_push($row, ord($value));
                    }
    
                    $jumlah_data = count($row) + 1;
    
                    // urutkan angka dari hasil konversi
                    for ($i = 1; $i < $jumlah_data; $i++) {
                        for ($j = 0; $j < $jumlah_data - 1; $j++) {
                            if ($row[$j] > $row[$j + 1]) {
                                $temp = $row[$j];
                                $row[$j] = $row[$j + 1];
                                $row[$j + 1] = $temp;
                            }
                        }
                    }
    
                    array_shift($row);
    
                    // kembalikan nilai dari angka ke huruf
                    $final_row = array();
                    foreach ($row as $index => $value) {
                        array_push($final_row, chr($value));
                    }
    
                    return $final_row;
                }
                ?>
            </samp>
        </div>
    </div>

    <div class="footer">
        Insya Allah Lulus, aamiin :D
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>