<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Dumbways Batch 13 Kloter 4 - Abdillah F.</title>

    <style>
        .footer {
            position: fixed;
            padding: 10px;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #e91e63;
            color: white;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h3>Soal 2 Dumbways Batch 13 Kloter 4</h3>
                <hr>
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="total-belanja">Masukkan Total Belanja</label>
                        <input type="number" id="total-belanja" name="total-belanja" class="form-control">
                        <small class="form-text text-muted">Nilai total belanja</small>
                    </div>
                    <div class="form-group">
                        <label for="nominal-pembayaran">Masukkan nominal pembayaran</label>
                        <input type="number" id="nominal-pembayaran" name="nominal-pembayaran" class="form-control">
                        <small class="form-text text-muted">Total nominal yang di masukkan untuk melakukan pembayaran</small>
                    </div>
                    <br>
                    <input class="btn btn-primary" type="submit" name="submit" value="Hitung Kembalian">
                </form>
            </div>
        </div>
        <br>
        <hr>
        <code><b>Result</b></code>
        <div>
            <samp>
                <?php
                if (isset($_POST["submit"])) {
                    $total_belanja = isset($_POST["total-belanja"]) ? (int) $_POST["total-belanja"] : 0;
                    $nominal_pembayaran = isset($_POST["nominal-pembayaran"]) ? (int) $_POST["nominal-pembayaran"] : 0;

                    // validasi nilai
                    if ($total_belanja == 0 && $nominal_pembayaran == 0) die("Total belanja dan nominal pembayaran Rp. 0");
                    if ($total_belanja == 0) die("Belanja Rp. 0");
                    if ($nominal_pembayaran == 0) die("Nominal pembayaran Rp. 0");

                    // pastikan bahwa nominal pembayran > total_belanja
                    if ($nominal_pembayaran < $total_belanja) die("Maaf, Uang yang anda bayarkan kuarang!");

                    echo "Total Belanja adalah $total_belanja";
                    echo "<br>";
                    echo "Nominal Pembayaran adalah $nominal_pembayaran";
                    echo "<br>";
                    $kembalian_list = kembalian($nominal_pembayaran, $total_belanja);

                    foreach ($kembalian_list as $key => $value) {
                        if ($value == 0) continue;
                        echo "$key = $value <br>";
                    }
                } else {
                    echo "Hasil belum tersedia";
                }

                // function kembalian
                function kembalian($pembayaran = 0, $belanja = 0)
                {
                    $kembalian = $pembayaran - $belanja;
                    $total_kembalian = $kembalian;

                    $jumlah_seratus_ribu = intval($kembalian / 100000);
                    $kembalian = $kembalian % 100000;

                    $jumlah_limapuluh_ribu = intval($kembalian / 50000);
                    $kembalian = $kembalian % 50000;

                    $jumlah_duapuluh_ribu = intval($kembalian / 20000);
                    $kembalian = $kembalian % 20000;

                    $jumlah_sepuluh_ribu = intval($kembalian / 10000);
                    $kembalian = $kembalian % 10000;

                    $jumlah_lima_ribu = intval($kembalian / 5000);
                    $kembalian = $kembalian % 5000;

                    $jumlah_dua_ribu = intval($kembalian / 2000);
                    $kembalian = $kembalian % 2000;

                    $jumlah_seribu = intval($kembalian / 1000);
                    $kembalian = $kembalian % 1000;

                    $jumlah_lima_ratus = intval($kembalian / 500);
                    $kembalian = $kembalian % 500;

                    $jumlah_dua_ratus = intval($kembalian / 200);
                    $kembalian = $kembalian % 200;

                    $jumlah_seratus = intval($kembalian / 100);
                    $kembalian = $kembalian % 100;

                    $final_kembalian = array(
                        "total_kembalian" => $total_kembalian,
                        100000 => $jumlah_seratus_ribu,
                        50000 => $jumlah_limapuluh_ribu,
                        20000 => $jumlah_duapuluh_ribu,
                        10000 => $jumlah_sepuluh_ribu,
                        5000 => $jumlah_lima_ribu,
                        2000 => $jumlah_dua_ribu,
                        1000 => $jumlah_seribu,
                        500 => $jumlah_lima_ratus,
                        200 => $jumlah_dua_ratus,
                        100 => $jumlah_seratus,
                    );

                    return $final_kembalian;
                }
                ?>
            </samp>
        </div>
    </div>

    <div class="footer">
        Insya Allah Lulus, aamiin :D
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>