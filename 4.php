<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Dumbways Batch 13 Kloter 4 - Abdillah F.</title>

    <style>
        .footer {
            position: fixed;
            padding: 10px;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #e91e63;
            color: white;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h3>Soal 4 Dumbways Batch 13 Kloter 14</h3>
                <hr>
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="text">Masukkan Text</label>
                        <input type="text" id="text" name="text" class="form-control">
                        <small class="form-text text-muted">Text yang ingin di enkripsi/dekripsi</small>
                    </div>
                    <div class="form-group">
                        <label for="key">Masukkan Key</label>
                        <input type="text" id="key" name="key" class="form-control">
                        <small class="form-text text-muted">kunci untuk membuka enkripsi/dekripsi</small>
                    </div>
                    <br>
                    <input class="btn btn-primary" type="submit" name="encrypt" value="Enkripsi">
                    <input class="btn btn-primary" type="submit" name="decrypt" value="Dekripsi">
                </form>
            </div>
        </div>
        <br>
        <hr>
        <code><b>Result</b></code>
        <div>
            <samp>
                <?php
                $text = isset($_POST["text"]) ? $_POST["text"] : "";
                $key = isset($_POST["key"]) ? $_POST["key"] : "";

                if (isset($_POST["encrypt"])) {
                    if (empty($text) && empty($key)) die("Empty text and key");
                    if (empty($text)) die("Empty text");
                    if (empty($key)) die("Empty key");

                    echo "ENKRIPSI : ";
                    echo "<br>";
                    echo "Text : $text";
                    echo "<br>";
                    echo "Key : $key";
                    echo "<br>";

                    echo "Hasil : <br>";
                    encrypt($text, $key);
                } elseif (isset($_POST["decrypt"])) {
                    if (empty($text) && empty($key)) die("Empty text and key");
                    if (empty($text)) die("Empty text");
                    if (empty($key)) die("Empty key");

                    echo "DEKRIPSI : ";
                    echo "<br>";
                    echo "Text : $text";
                    echo "<br>";
                    echo "Key : $key";
                    echo "<br>";

                    echo "Hasil : <br>";
                    decrypt($text, $key);
                } else {
                    echo "Hasil belum tersedia";
                }

                function encrypt($text = "", $key = "")
                {
                    $text_len = strlen($text);
                    $key_len = strlen($key);

                    $split_text = str_split($text);
                    $split_key = str_split($key);

                    $i = 0;
                    for ($j = 0; $j < $text_len; $j++) {
                        if ($i == $key_len) {
                            $i = 0;
                        }
                        $split_key2[$j] = $split_key[$i];
                        $i++;
                    }

                    for ($k = 0; $k < $text_len; $k++) {
                        $a = char_to_dec($split_key2[$k]);
                        $b = char_to_dec($split_text[$k]);
                        if (($a && $b) != null) {
                            echo (tabel_vigenere_encrypt($a, $b));
                        } else {
                            echo $split_text[$k];
                        }
                    }
                }

                function decrypt($text = "", $key = "")
                {
                    $text_len = strlen($text);
                    $key_len = strlen($key);

                    $split_text = str_split($text);
                    $split_key = str_split($key);

                    $i = 0;
                    for ($j = 0; $j < $text_len; $j++) {
                        if ($i == $key_len) {
                            $i = 0;
                        }
                        $split_key2[$j] = $split_key[$i];
                        $i++;
                    }

                    for ($k = 0; $k < $text_len; $k++) {
                        $a = char_to_dec($split_key2[$k]);
                        $b = char_to_dec($split_text[$k]);
                        if (($a && $b) != null) {
                            echo (tabel_vigenere_decrypt($b, $a));
                        } else {
                            echo $split_text[$k];
                        }
                    }
                }

                function char_to_dec($a)
                {
                    $i = ord($a);
                    if ($i >= 97 && $i <= 122) {
                        return ($i - 96);
                    } else if ($i >= 65 && $i <= 90) {
                        return ($i - 38);
                    } else {
                        return null;
                    }
                }

                function dec_to_char($a)
                {
                    if ($a >= 1 && $a <= 26) {
                        return (chr($a + 96));
                    } else if ($a >= 27 && $a <= 52) {
                        return (chr($a + 38));
                    } else {
                        return null;
                    }
                }

                function tabel_vigenere_encrypt($a, $b)
                {
                    $i = $a + $b - 1;
                    if ($i > 26) {
                        $i = $i - 26;
                    }
                    return (dec_to_char($i));
                }
                function tabel_vigenere_decrypt($a, $b)
                {
                    $i = $a - $b + 1;
                    if ($i < 1) {
                        $i = $i + 26;
                    }
                    return (dec_to_char($i));
                }
                ?>
            </samp>
        </div>
    </div>

    <div class="footer">
        Insya Allah Lulus, aamiin :D
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>