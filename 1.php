<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Dumbways Batch 13 Kloter 4 - Abdillah F.</title>

    <style>
        .footer {
            position: fixed;
            padding: 10px;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #e91e63;
            color: white;
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h3>Soal 1 Dumbways Batch 13 Kloter 4</h3>
                <hr>
                <form action="" method="POST">
                    <label for="deret-angka">Masukkan deret angka</label>
                    <input type="text" id="deret-angka" name="deret-angka" class="form-control">
                    <small class="form-text text-muted">Pisahkan setiap angka dengan koma tanpa menggunakan spasi. Misal 1, 2, 3, 4</small>
                    <br>
                    <input class="btn btn-primary" type="submit" name="submit" value="Hitung">
                </form>
            </div>
        </div>
        <br>
        <hr>
        <code><b>Result</b></code>
        <div>
            <samp>
                <?php
                if (isset($_POST["submit"])) {
                    $deret_angka = isset($_POST["deret-angka"]) ? trim($_POST["deret-angka"]) : "";

                    // check apakah deret angka diisi atau tidak
                    if (empty($deret_angka)) die("Deret angka kosong");

                    // check apakah deret angka ada value nilai selain angka, space dan tanda koma
                    // /^[0-9.,- ]*$/
                    if (!preg_match("/^[0-9.,-]*$/", $deret_angka)) die("Karakter yang diizinkan hanya angka, titik, koma dan tanda minus");

                    $deret_angka_array = explode(",", $deret_angka);

                    $panjang_deret_angka_array = count($deret_angka_array);

                    $total_deret_angka_list = array();

                    for ($i = 0; $i <= ($panjang_deret_angka_array - 1); $i++) {

                        $no_perulangan = 0;
                        $deret_jumlah_angka = "";
                        $total_deret_angka = 0;

                        foreach ($deret_angka_array as $index => $value) {
                            if ($value == "") die("Inputan tidak sesuai, pastikan inputan anda sudah benar!");

                            $no_perulangan++;

                            if ($i == $index) {
                                $value_cetak = "";
                            } else {
                                $value_cetak = "$value";
                            }

                            $deret_jumlah_angka = $deret_jumlah_angka . " $value_cetak ";

                            if ($i != $index) {
                                $total_deret_angka += $value;
                            }

                            if ($no_perulangan == $panjang_deret_angka_array) {

                                // tampung $total_deret_angka ke dalam $total_deret_angka_list
                                array_push($total_deret_angka_list, $total_deret_angka);

                                echo " Deret Angka " . ($i + 1) . " : $deret_jumlah_angka = " . $total_deret_angka;
                            } else {
                                continue;
                            }
                        }
                        echo "<br>";
                    }
                    echo "<br>";
                    echo "Maka Angka terkecil dari hasil penjumlahan deret angka adalah " . min($total_deret_angka_list);
                    echo "<br>dan Angka terbesar dari hasil penjumlahan deret angka adalah " . max($total_deret_angka_list);
                } else {
                    echo "Hasil belum tersedia";
                }
                ?>
            </samp>
        </div>
    </div>

    <div class="footer">
        Insya Allah Lulus, aamiin :D
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>